package com.tangit.PhoneGapDemoGPS;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.phonegap.DroidGap;

public class PhoneGapDemo_03_GPSActivity extends DroidGap {
	ProgressDialog progDial;	// プログレスダイアログオブジェクト
    @Override
    public void onCreate(Bundle savedInstanceState) {

    	// ダイアログを表示
		progDial = new ProgressDialog(this);
		progDial.setTitle("Now Loading...");
		progDial.setMessage("コンテンツを読み込んでいます。");
		progDial.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progDial.setCancelable(true);
		progDial.show();

        super.onCreate(savedInstanceState);
        super.loadUrl("file:///android_asset/www/index.html");

        // JavaScriptから呼び出し可能なネイティブ拡張クラスを実装
        super.appView.addJavascriptInterface(new NativeExtensions(this), "_extensions");
    }

    // ネイティブ拡張クラス
    public class NativeExtensions
    {
    	private Activity parentActivity; // 親のアクティビティ

    	// コンストラクタ
    	public NativeExtensions(Activity _act){
    		this.parentActivity = _act;
    	}

    	// ダイアログを消す。
		public void DisposeProgress() {
			progDial.dismiss();
		}

		// ルート案内をする。
		public void navigateRoot(final String position) {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
			intent.setData(Uri.parse("http://maps.google.com/maps?myl=saddr&daddr=" + position + "&dirflg=d"));
			this.parentActivity.startActivity(intent);
		}
    }
}
